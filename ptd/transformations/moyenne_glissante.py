import string
from ptd.transformations.transformation import Transformation
from ptd.model.tableau import Tableau 
import doctest

class MoyenneGlissante(Transformation):
    """
    Classe qui effectue moyenne glissante sur une table, la fenetre peut être paire ou impaire. Si elle est paire on considère la valeur supérieure en prioritée.

    Attributes
    ----------
    fenetre : int
        fenetre pour le calcule de la moyenne glissante
    nom_variable : str
        variable sur laquelle on effectue la moyenne glissante

    --------
    Examples :
    >>> MG1 = MoyenneGlissante(2,"age")
    >>> T1 = Tableau(["taille","age","poids"],[[160,12,40],[180,8,80],[12,12,16]])
	>>> print(MG1.transformation(T1))
	['taille', 'age', 'poids', 'moy_gliss_age'], [[160, 12, 40, None], [180, 8, 80, 10.0], [12, 12, 16, None]]
    >>> MG2 = MoyenneGlissante(2,"sexe")
    >>> T2 = Tableau(["taille","age","poids"],[[160,12,40],[180,8,80],[12,10,16]])
    >>> print(MG2.transformation(T2))
    La colonne spécifiée n'est pas dans la table
    >>> MG3 = MoyenneGlissante(3,"age")
    >>> T3 = Tableau(["taille","age","poids"],[[160,12,40],[180,8,80],[12,10,16]])
	>>> print(MG3.transformation(T3))
	['taille', 'age', 'poids', 'moy_gliss_age'], [[160, 12, 40, None], [180, 8, 80, 10.0], [12, 10, 16, None]]
    >>> MG4 = MoyenneGlissante(3,"age")
    >>> T4 = Tableau(["taille","age","poids"],[[160,None,40],[180,8,80],[12,10,16]])
	>>> print(MG3.transformation(T4))
	['taille', 'age', 'poids', 'moy_gliss_age'], [[160, None, 40, None], [180, 8, 80, None], [12, 10, 16, None]]

    """

    def __init__(self, fenetre, nom_variable):
        """Initialise la classe"""
        super().__init__()
        self.fenetre = fenetre
        self.nom_variable = nom_variable 

    def transformation(self, tableau = Tableau([],[[]])):
        """Crée une nouvelle colonne avec la moyenne glissante

        Pour les exemples d'utilisation, voir la documentation de la classe MoyenneGlissante

        Parameters
        ----------
        table : Tableau
            tableau où se trouve la variable pour laquelle on veut faire la moyenne glissante
        
        Returns
        -------
        Tableau
            table avec la nouvelle colonne
        """
        T = tableau
        L=[]
        compteur_bis = 0
        if self.nom_variable not in tableau.header :
            return "La colonne spécifiée n'est pas dans la table"    
        for i in range(len(tableau.header)):
            if tableau.header[i] == self.nom_variable :                   
                for ligne in tableau.body :
                    compteur_bis = compteur_bis + 1
                    if self.fenetre % 2 == 1 :    #Cas si la fenetre est impaire
                        m = self.fenetre // 2          
                        S = 0 
                        if compteur_bis -1 < m or len(tableau.body) - compteur_bis < m :
                            L.append(None)   #impossible de calculer (pas dans la fenetre)
                        else :
                            for x in range(compteur_bis-1 - m, compteur_bis + m):
                                if type(tableau.body[x][i]) == int or type(tableau.body[x][i]) == float :
                                    S = S + tableau.body[x][i]
                                else:
                                    L.append(None)
                                    break
                            M = S/self.fenetre 
                            L.append(M)
                            if L[-1] == 0.0 :    #problématique si il y a une moyenne de 0.0
                                L.pop()
                    else :                  #Cas si la fenetre est paire       
                        m = self.fenetre // 2
                        S = 0 
                        if compteur_bis -1 < m or len(tableau.body) - compteur_bis < m :
                            L.append(None)                #impossible de calculer (pas dans la fenetre)
                        else :
                            for x in range(compteur_bis - m, compteur_bis + m): 
                                if type(tableau.body[x][i]) == int or type(tableau.body[x][i]) == float :    
                                    S = S + tableau.body[x][i]
                                else:
                                    L.append(None)
                                    break                           
                            M = S/self.fenetre 
                            L.append(M)
                            if L[-1] == 0.0 :
                                L.pop()
                T.add_header("moy_gliss_"+self.nom_variable)
                T.add_column(L)
        
        return T


if __name__ == "__main__" :
    doctest.testmod(verbose=True)