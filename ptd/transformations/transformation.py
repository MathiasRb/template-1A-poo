from ptd.model.tableau import Tableau

class Transformation: 
    """
    Classe-mère abstraite implémentant une transformation. 
    """
    def __init__(self):
        pass

    def transformation(self,table=Tableau([],[[]])):
        pass