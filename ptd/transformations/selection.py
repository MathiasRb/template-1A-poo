from ptd.transformations.transformation import Transformation 
from ptd.model.tableau import Tableau 
import doctest

class Selection(Transformation):
    """
    Classe qui permet de sélectionner une ou plusieurs variables d'un tableau

    Attributes
    ----------
    noms_variables : list
        liste des variables qu'on veut sélectionner

    Examples 
    --------
    >>> S = Selection(["taille","poids"])
    >>> T = Tableau(["taille","age","poids"],[[160,12,40],[180,67,80],[12,14,16]])
    >>> print(S.transformation(T))
    ['taille', 'poids'], [[160, 40], [180, 80], [12, 16]]
    >>> T2 = Tableau(["taille","age","poids"],[[160,12,"hello"],[180,67,80],[None,14,16]])
    >>> print(S.transformation(T2))
    ['taille', 'poids'], [[160, 'hello'], [180, 80], [None, 16]]
    >>> S3 = Selection(["sexe"])
    >>> T3 = Tableau(["taille","age","poids"],[[160,12,40],[180,67,80],[12,14,16]])
    >>> print(S3.transformation(T3))
    La colonne spécifiée n'existe pas


    """
    def __init__(self,noms_variables):
        """Initialise la classe"""
        super().__init__()
        self.noms_variables = noms_variables

    
    def transformation(self, table=Tableau([],[[]])):
        """Conserve uniquement les variables d'intérêts

        Pour les exemples d'utilisation, voir la documentation de la classe Selection

        Parameters
        ----------
        table : Tableau
            table pour laquelle on souhaite selectionner des variables
        
        Returns
        -------
        Tableau
            table avec seulement les colonnes d'intérêts
        """
        T= Tableau([],[[]])
        for i in range(len(self.noms_variables)) :
            if self.noms_variables[i] not in table.header :
                return "La colonne spécifiée n'existe pas"    #Si une des variables spécifiée en entrée n'existe pas le programme s'arrête.
            for j in range(len(table.header)) :    
                if self.noms_variables[i] == table.header[j] :
                    L=[]
                    for x in table.body :
                        L.append(x[j])
                    T.add_header(self.noms_variables[i])
                    T.add_column(L)
        return T


if __name__ == "__main__" :
    doctest.testmod(verbose=True)

