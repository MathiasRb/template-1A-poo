from ptd.transformations.transformation import Transformation
from ptd.model.tableau import Tableau
import doctest

class AgregationSpatiale(Transformation):
    """Classe implémentant l'agrégation spatiale.

    La classe permet d'agréger les données d'un tableau par les valeurs d'une colonne spécifiée.
    Pour chaque colonne du tableau initial, on applique les fonctions d'agrégation données pour déterminer la valeur agrégée de chaque cellule de la colonne du tableau agrégé.

    Attributes
    ----------
    noms_variables : list[str]
        liste des noms de colonnes (telles qu'elles sont nommées dans le header) que l'on souhaite conserver et agréger (sauf colonne_agregation)
    fonctions_agregation : list[function]
        liste des fonctions à appliquer pour chaque colonne mentionnée dans noms_variables, donc de la même taille que noms_variables ; une fonction d'agrégation prend une liste en entrée et retourne une valeur (ex: sum, max, statistics.mean, lambda x: "mq" in x, etc.)
    colonne_agregation : str
        nom de la colonne (telle qu'elle est nommée dans le header) dans laquelle l'égalité de deux éléments donnera lieu à une agrégation des deux lignes correspondantes. Par exemple, une agrégation des données par région se fera en utilisant la colonne d'agrégation 'region'

    Examples
    --------
    >>> table = Tableau(["id", "valeur", "quantite", "pays"],[["1",10,7,"France"],["2", 15,9,"Italie"],["3", 13,4,"France"],["4", 16,5, "Suisse"]])
    >>> AS = AgregationSpatiale(["valeur", "quantite"], [max, sum], "pays")
    >>> print(AS.transformation(table))
    ['valeur', 'quantite', 'pays'], [[13, 11, 'France'], [15, 9, 'Italie'], [16, 5, 'Suisse']]

    >>> AS = AgregationSpatiale(["valeur", "quantite"], [max, sum])
    >>> print(AS.transformation(table))
    La colonne spécifiée pour l'agrégation n'existe pas. L'agrégation sera donc globale : toutes les valeurs du tableau seront agrégées par colonne et formeront un tableau avec une seule ligne.
    ['valeur', 'quantite', 'colonne_agregation'], [[16, 25, 'everything']]

    """
    def __init__(self, noms_variables, fonctions_agregation, colonne_agregation=""):
        """Initialise la classe AgregationSpatiale"""
        super().__init__()
        self.noms_variables=noms_variables
        self.fonctions_agregation=fonctions_agregation
        self.colonne_agregation = colonne_agregation
    
    def transformation(self, table=Tableau([],[[]])):
        """Renvoie la table agrégée selon les valeurs d'une colonne donnée via des fonctions d'agrégations données

        Pour les exemples d'utilisation, voir la documentation de la classe AgregationSpatiale

        Parameters
        ----------
        table : Tableau
            table que l'on souhaite agréger
        
        Returns
        -------
        Tableau
            table agrégée
        """

        #Tests basiques sur la faisabilité de l'agrégation : en cas de problème, on renvoie la table initiale, sans l'agréger, et on renvoie un message expliquant le problème
        for variable in self.noms_variables:
            if variable not in table.header:
                print("La variable '"+variable+"' à agréger n'existe pas.")
                return table
        if len(self.noms_variables) < len(self.fonctions_agregation):
            print("Erreur : pas assez de variables choisies pour le nombre de fonctions d'agrégation indiquées.")
            return table
        elif len(self.noms_variables) > len(self.fonctions_agregation):
            print("Erreur : pas assez de fonctions d'agrégation choisies pour le nombre de variables indiquées.")
            return table
        elif self.colonne_agregation in self.noms_variables:
            print("Impossible d'appliquer une fonction d'agrégation spécifique sur la colonne qui sert pour l'agrégation.")
            return table
            

        else:
            #Gestion du cas où l'utilisateur veut tout agréger pour obtenir une seule ligne agrégée dans son tableau
            if self.colonne_agregation not in table.header:
                print("La colonne spécifiée pour l'agrégation n'existe pas. L'agrégation sera donc globale : toutes les valeurs du tableau seront agrégées par colonne et formeront un tableau avec une seule ligne.")
                table.add_header("colonne_agregation")
                table.add_column(["everything" for i in range(len(table.body))])

                self.colonne_agregation = "colonne_agregation"

            numCol = table.header.index(self.colonne_agregation)

            numColVariables = []
            for var in self.noms_variables:
                numColVariables.append(table.header.index(var))

            #construction du header pour les variables que l'utilisateur souhaite conserver et agréger
            headertableAgregee = []
            for numVar in numColVariables:
                headertableAgregee.append(table.header[numVar])
            headertableAgregee.append(self.colonne_agregation)

            tableAgregee = Tableau(self.noms_variables+[self.colonne_agregation],[])
            
            # inventaire des lignes qui doivent être regroupées pour être agrégées
            regroupements = [] # liste de tuples de type (variable_agregative, [listes d'index de lignes correspondantes])
            for i in range(len(table.body)):
                valeur_agregative = table.body[i][numCol]
                liste_valeurs_agregatives = [x[0] for x in regroupements]
                if valeur_agregative in liste_valeurs_agregatives: #si la valeur agrégative (par exemple, un nom de région) a déjà été répertoriée dans les itérations précédentes
                    indice = liste_valeurs_agregatives.index(valeur_agregative)
                    regroupements[indice][1].append(i)
                else:
                    regroupements.append((valeur_agregative, [i])) #nouvelle valeur agrégative trouvée et ajoutée dans la liste des regroupements avec son numéro de ligne associé

            #détermination des valeurs d'agrégation et affectation de la valeur dans la table agrégée
            for reg in regroupements:
                valeurs_agregees = []
                j= -1
                for var in numColVariables:
                    j+=1
                    listedevaleurs = [table.body[i][var] for i in reg[1]]
                    valeur_agregee = self.fonctions_agregation[j](listedevaleurs)
                    valeurs_agregees.append(valeur_agregee)
                tableAgregee.add_row(valeurs_agregees+[reg[0]])
        
        return tableAgregee

if __name__ == "__main__":
    doctest.testmod(verbose=True)