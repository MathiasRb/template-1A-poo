from ptd.transformations.transformation import Transformation
from ptd.model.tableau import Tableau
import doctest
from math import sqrt

class Normalisation(Transformation):
    """
    Classe permettant de normaliser, c'est-à-dire centrer et réduire, les variables d'intérêts

    Attributes
    ----------
    nom_variable : str
        nom de la variable pour laquelle on souhaite effectuer une normalisation 
    
    Examples
    --------

    >>> T=Tableau(["var"],[[0],[1],[2]])
    >>> norm=Normalisation("var")
    >>> sqrt(3/2)==1.224744871391589
    True
    >>> print(norm.transformation(T))
    ['var', 'var_normalisé(e)'], [[0, -1.224744871391589], [1, 0.0], [2, 1.224744871391589]]
    
    """
    def __init__(self, nom_variable):
        """Initialise la classe"""
        super().__init__()
        self.nom_variable=nom_variable

    def transformation(self, table=Tableau([],[[]])):
        """ajoute une colonne qui est les valeurs centrées réduites de la variable choisi

        Pour les exemples d'utilisation, voir la documentation de la classe Normalisation

        Parameters
        ----------
        table : Tableau
            table pour laquelle on souhaite normaliser une variable
        
        Returns
        -------
        Tableau
            table avec la nouvelle colonne
        """
        if self.nom_variable not in table.header:
                raise NameError("La variable renseignée n'existe pas")
        m=0
        n=len(table.body)
        indice=0
        compteur=0
        L=[]
        v=0
        while table.header[indice]!= self.nom_variable: #on cherche l'indice de la variable
            indice+=1

        for i in range(n): #on calcule la moyenne
            if table.body[i][indice]!=None:
                m+=table.body[i][indice]
                compteur+=1
        m=m/compteur 

        for i in range(n): #on calcule la variance
            if table.body[i][indice]!=None:
                v+=(table.body[i][indice]-m)**2
        v=v/compteur

        for i in range(n): #on construit la liste avec les valeurs normalisées
            if table.body[i][indice]!=None:
                L.append((table.body[i][indice]-m)/sqrt(v))
            else:
                L.append(None)

        table.add_header(self.nom_variable+ "_normalisé(e)")
        table.add_column(L)
        return table

if __name__ == "__main__":
    doctest.testmod(verbose=True)