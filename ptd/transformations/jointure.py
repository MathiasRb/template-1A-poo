from ptd.transformations.transformation import Transformation
from ptd.model.tableau import Tableau
import doctest

class Jointure(Transformation):
    """Classe implémentant la jointure (externe bilatérale) entre deux tables selon une colonne de jointure spécifiée

    Attributes
    ----------
    autre_table : Tableau
        table que l'on souhaite fusionner à la première
    col_tableA : str
        nom de la colonne (telle qu'elle est nommée dans le header) servant à la jointure dans la première table
    col_tableB : str
        nom de la colonne (telle qu'elle est nommée dans le header) servant à la jointure dans la deuxième table

    Examples
    --------
    >>> tableA = Tableau(['pays', 'codepays'], [['France', 'FRA'], ['Italie', 'ITA'], ['Etats-Unis', 'USA']])
    >>> tableB = Tableau(['nom_pays', 'capitale', 'continent'], [['Etats-Unis', 'Washington', 'Amérique'], ['France','Paris', 'Europe'], ['Italie', 'Rome', 'Europe']])
    >>> jointure = Jointure(tableB, 'pays', 'nom_pays')
    >>> tablejointe = jointure.transformation(tableA)
    >>> print(tablejointe)
    ['pays', 'codepays', 'capitale', 'continent'], [['France', 'FRA', 'Paris', 'Europe'], ['Italie', 'ITA', 'Rome', 'Europe'], ['Etats-Unis', 'USA', 'Washington', 'Amérique']]

    >>> tableC = Tableau(['pays', 'codepays'], [['France', 'FRA'], ['Italie', 'ITA'], ['Portugal', 'POR']])
    >>> tableD = Tableau(['nom_pays', 'capitale', 'continent'], [['Espagne', 'Madrid', 'Europe'], ['France','Paris', 'Europe'], ['Portugal', 'Lisbonne', 'Europe']])
    >>> jointure = Jointure(tableD, 'pays', 'nom_pays')
    >>> tablejointe = jointure.transformation(tableC)
    Attention, le contenu des tables pour la colonne spécifiée n'est pas le même pour les deux tables. Jointure imparfaite à prévoir.
    >>> print(tablejointe)
    ['pays', 'codepays', 'capitale', 'continent'], [['France', 'FRA', 'Paris', 'Europe'], ['Portugal', 'POR', 'Lisbonne', 'Europe'], ['Italie', 'ITA', None, None], ['Espagne', None, 'Madrid', 'Europe']]

    """
    def __init__(self, autre_table, col_tableA, col_tableB):
        """Initialise la classe Jointure"""
        super().__init__()
        self.autre_table = autre_table
        self.col_tableA = col_tableA
        self.col_tableB = col_tableB

    def lignes_compatibles(self, numColA, numColB, table=Tableau([],[[]]), ):
        """Renvoie les couples de lignes pour lesquelles la fusion "parfaite" (sans None) entre les deux lignes est possible

        Parameters
        ----------
        numColA : int
            indice de la colonne servant pour la jointure pour la première table
        numColB : int
            indice de la colonne servant pour la jointure pour la deuxième table
        table : Tableau
            table que l'on souhaite fusionner à la deuxième 

        Returns
        -------
        list[tuple]
            Liste des combinaisons de lignes que l'on peut joindre sans occasionner de None (jointure parfaite)*
        
        Examples
        --------
        >>> tableC = Tableau(['pays', 'codepays'], [['France', 'FRA'], ['Italie', 'ITA'], ['Portugal', 'POR']])
        >>> tableD = Tableau(['nom_pays', 'capitale', 'continent'], [['Espagne', 'Madrid', 'Europe'], ['France','Paris', 'Europe'], ['Portugal', 'Lisbonne', 'Europe']])
        >>> jointure = Jointure(tableD, 'pays', 'nom_pays')
        >>> compatibles = jointure.lignes_compatibles(0, 0, tableC)
        >>> print(compatibles)
        [(0, 1), (2, 2)]
        """
        tableA = table
        tableB = self.autre_table

        #Tests d'unicité des valeurs dans les deux colonnes respectives de jointure
        liste_colonne_A = [x[numColA] for x in tableA.body]
        liste_colonne_B =[x[numColB] for x in tableB.body]
        if len(set(liste_colonne_A)) != len(liste_colonne_A):
            print("Erreur : jointure impossible car les données de votre premier tableau ne sont pas uniques.")
            return None
        elif len(set(liste_colonne_B)) != len(liste_colonne_B):
            print("Erreur : jointure impossible car les données de votre deuxième tableau ne sont pas uniques.")
            return None


        lignes_compatibles=[]
        for i in range(len(tableA.body)):
            for j in range(len(tableB.body)):
                if tableA.body[i][numColA] == tableB.body[j][numColB]: #cherche s'il y a correspondance entre un item de la colonne A et de la colonne B
                    lignes_compatibles.append((i,j))

        return lignes_compatibles
            


    def transformation(self, table=Tableau([],[[]])):
        """Renvoie la jointure (externe bilatérale) entre deux tables

        Pour les exemples d'utilisation, voir la documentation de la classe Jointure

        Parameters
        ----------
        table : Tableau
            table que l'on souhaite fusionner à la deuxième
        
        Returns
        -------
        Tableau
            table fusionnée
        """

        tableA = table
        tableB = self.autre_table

        if self.col_tableA not in tableA.header or self.col_tableB not in tableB.header:
            print("Les colonnes spécifiées pour la jointure n'existent pas")
            return tableA
        else:
            numColA = tableA.header.index(self.col_tableA)
            numColB = tableB.header.index(self.col_tableB)

            #Test préliminaire pour savoir si les éléments de la colonne A sont identiques à la colonne B, ce qui permettrait une jointure sans aucune valeur manquante
            liste_colonne_A = [x[numColA] for x in tableA.body]
            liste_colonne_B =[x[numColB] for x in tableB.body]
            if sorted(liste_colonne_A) != sorted(liste_colonne_B):
                print("Attention, le contenu des tables pour la colonne spécifiée n'est pas le même pour les deux tables. Jointure imparfaite à prévoir.")

            tableFusionnee = Tableau(tableA.header + tableB.header, [])
            compatibles = self.lignes_compatibles(numColA, numColB, tableA) #détermination des couples d'indices de lignes compatibles pour les colonnes A et B
            for tupl in compatibles:
                tableFusionnee.add_row(tableA.body[tupl[0]]+tableB.body[tupl[1]]) #ajout progressif des lignes correspondantes
                
            # Ajout des lignes qui ne correspondent pas avec les éléments de l'autre tableau : apparition de None pour les lignes où la jointure ne peut pas être réalisée correctement
            for i in range(len(tableA.body)):
                if i not in [x[0] for x in compatibles]:
                    tableFusionnee.add_row(tableA.body[i]+[None for i in range(len(tableB.header))])
            for i in range(len(tableB.body)):
                if i not in [x[1] for x in compatibles]:
                    tableFusionnee.add_row([None for i in range(len(tableA.header))]+tableB.body[i])

                


            #on gère le cas des jointures imparfaites où un None est apparu dans la colonne A durant la fusion, mais comme on s'apprête à supprimer la colonne B, on transfère l'information de la colonne B dans la colonne A avant la suppression
            numColA_Fus = tableFusionnee.header.index(self.col_tableA)
            numColB_Fus = tableFusionnee.header.index(self.col_tableB)
            for ligne in tableFusionnee.body:
                if ligne[numColA_Fus] == None and ligne[numColB_Fus] != None:
                    ligne[numColA_Fus] = ligne[numColB_Fus]
                        
            #on supprime la colonne B qui est désormais un doublon de la colonne A
            tableFusionnee.del_column(self.col_tableB)
            tableFusionnee.del_header(self.col_tableB)
        
        return tableFusionnee


if __name__ == "__main__":
    doctest.testmod(verbose=True)