from ptd.transformations.transformation import Transformation
from ptd.model.tableau import Tableau
import doctest

class Centrage(Transformation):
    """
    Classe permettant de centrer les variables d'intérêts

    Attributes
    ----------
    nom_variable : str
        nom de la variable pour laquelle on souhaite la centrer

    Examples
    --------
    
    >>> T=Tableau(["var"],[[0],[1],[2],[3],[4],[5],[6],[7],[8],[9],[None]])
    >>> centre=Centrage("var")
    >>> print(centre.transformation(T))
    ['var', 'var_centré(e)'], [[0, -4.5], [1, -3.5], [2, -2.5], [3, -1.5], [4, -0.5], [5, 0.5], [6, 1.5], [7, 2.5], [8, 3.5], [9, 4.5], [None, None]]
    """
    def __init__(self, nom_variable):
        """Initialise la classe"""
        super().__init__()
        self.nom_variable=nom_variable

    def transformation(self, table=Tableau([],[[]])):
        """ajoute une colonne qui est les valeurs centrées de la variable choisi

        Pour les exemples d'utilisation, voir la documentation de la classe Centrage

        Parameters
        ----------
        table : Tableau
            table pour laquelle on souhaite centrer une variable
        
        Returns
        -------
        Tableau
            table avec la nouvelle colonne
        """
        if self.nom_variable not in table.header:
                raise NameError("La variable renseignée n'existe pas")
        m=0
        n=len(table.body)
        indice=0
        compteur=0
        L=[]
        while table.header[indice]!= self.nom_variable: #on cherhe l'indice de la variable
            indice+=1

        for i in range(n): #on calcule la moyenne
            if table.body[i][indice]!=None:
                m+=table.body[i][indice]
                compteur+=1
        m=m/compteur

        for i in range(n): #on construit la nouvelle colonne
            if table.body[i][indice]!=None:
                L.append(table.body[i][indice]-m)
            else:
                L.append(None)

        table.add_header(self.nom_variable+ "_centré(e)")
        table.add_column(L)
        return table

if __name__ == "__main__":
    doctest.testmod(verbose=True)