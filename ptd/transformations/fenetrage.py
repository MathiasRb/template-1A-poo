from ptd.transformations.transformation import Transformation
from ptd.model.tableau import Tableau
from ptd.model.date import Date
import doctest
class Fenetrage(Transformation):
    """Classe permettant de sélectionner la période de temps pour une variable date donnée

    le format de la date devra être sous la forme:
    annee, mois, jours, heures, minutes, secondes tous concaténés
    exemple: 20130101210000

    Attributes:
    ----------

    nom_variable : str
        nom de la variable date du tableau
    debut : Date
        instance de Date représentant le début de la période temporelle d'intérêt
    fin : Date 
        instance de Date représentant la fin de la période temporelle d'intérêt

    Examples
    --------

    >>> T=Tableau(['date'],[['20130101210004'],['20140101210000'],['20150101210000'],['20100101210000'],['20080101210000'],['20130101190000'],['20130401210000'],['20130120210000'],['20130401215959']])
    >>> fenetre=Fenetrage('date',Date(2013,1,1,21,0,0), Date(2013,3,14,12,12,12))
    >>> print(fenetre.transformation(T))
    ['date'], [['20130101210004'], ['20130120210000']]
    """
    def __init__(self, nom_variable, debut, fin):
        """Initialise la classe"""
        super().__init__()
        self.nom_variable=nom_variable
        self.debut=debut
        self.fin=fin

    def transformation(self, table=Tableau([],[[]])):
        """selectionne les lignes pour lesquelles la date est dans l'intervalle

        Pour les exemples d'utilisation, voir la documentation de la classe Fenetrage

        Parameters
        ----------
        table : Tableau
            tableau qui va subir le fenêtrage
        
        Returns
        -------
        Tableau
            table après le fenêtrage
        """
        if self.nom_variable not in table.header:
                raise NameError("La variable renseignée n'existe pas")
        indice=0
        while table.header[indice]!= self.nom_variable: #on cherche l'indice de la variable
            indice+=1
        L=[]
        T=table.body
        for i in range(len(T)): #on construit une liste avec des instances de Date correspondant aux lignes de table
            if T[i][indice]!=None:
                date_i = str(T[i][indice])
                L.append(Date(int(date_i[0:4]),
                        int(date_i[4:6]),
                        int(date_i[6:8]),
                        int(date_i[8:10]),
                        int(date_i[10:12]),
                        int(date_i[12:14])))
        liste_annees=[]
        for i in range(len(T)-1,-1,-1): #on supprime les lignes pour lesquelles l'année n'appartient pas à l'intervalle
            if L[i].annee<self.debut.annee or L[i].annee>self.fin.annee:
                T.pop(i)
                L.pop(i)
            else:
                liste_annees.append(L[i].annee) 
        annee_min=min(liste_annees)
        annee_max=max(liste_annees)
        for i in range(len(T)-1,-1,-1):
            if L[i].annee==annee_min: #on supprime les lignes pour lesquelles la date est plus petite que la date du début
                if L[i].mois<self.debut.mois:
                    T.pop(i)
                    L.pop(i)
                elif L[i].mois==self.debut.mois:
                    if L[i].jour<self.debut.jour:
                        T.pop(i)
                        L.pop(i)
                    elif L[i].jour==self.debut.jour:
                        if L[i].heure<self.debut.heure:
                            T.pop(i)
                            L.pop(i)
                        elif L[i].jour==self.debut.jour:
                            if L[i].heure<self.debut.heure:
                                T.pop(i)
                                L.pop(i)
                            elif L[i].heure==self.debut.heure:
                                if L[i].minute<self.debut.minute:
                                    T.pop(i)
                                    L.pop(i)
                                elif L[i].minute==self.debut.minute:
                                    if L[i].seconde<self.debut.seconde:
                                        T.pop(i)
                                        L.pop(i)
            if L[i].annee==annee_max: #on supprime les lignes pour lesquelles la date est plus grande que la date de fin
                if L[i].mois>self.fin.mois:
                    T.pop(i)
                    L.pop(i)
                elif L[i].mois==self.fin.mois:
                    if L[i].jour>self.fin.jour:
                        T.pop(i)
                        L.pop(i)
                    elif L[i].jour==self.fin.jour:
                        if L[i].heure>self.fin.heure:
                            T.pop(i)
                            L.pop(i)
                        elif L[i].jour==self.fin.jour:
                            if L[i].heure>self.fin.heure:
                                T.pop(i)
                                L.pop(i)
                            elif L[i].heure==self.fin.heure:
                                if L[i].minute>self.fin.minute:
                                    T.pop(i)
                                    L.pop(i)
                                elif L[i].minute==self.fin.minute:
                                    if L[i].seconde>self.fin.seconde:
                                        T.pop(i)
                                        L.pop(i)

        return Tableau(table.header,T)


if __name__ == "__main__":
    doctest.testmod(verbose=True)