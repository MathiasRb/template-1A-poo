from ptd.transformations.transformation import Transformation
from ptd.model.tableau import Tableau
import doctest

class Filtrage(Transformation):

	"""
	Classe qui permet de sélectionner les lignes d'un tableau qui vérifient un filtre

	----------
	Attributes
	nom_variable : list
		variables sur lesquelles on utilise le filtre
	filtre : function
		fonction qui renvoie un booléen 

	--------
	Examples
	>>> F = Filtrage(["age"], lambda x : x>10)
    >>> T = Tableau(["taille","age","poids"],[[160,12,40],[180,8,80],[12,14,16]])
	>>> print(F.transformation(T))
	['taille', 'age', 'poids'], [[160, 12, 40], [12, 14, 16]]
	>>> F2 = Filtrage(["poids"], lambda x : x>60)
	>>> T2 = Tableau(["taille","age"],[[160,12],[180,8],[12,14]])
	>>> print(F2.transformation(T2))
	La colonne spécifiée n'existe pas
	>>> F3 = Filtrage(["age"], lambda x : x>10)
    >>> T3 = Tableau(["taille","age","poids"],[[160,12,40],[180,None,80],[12,14,16]])
	>>> print(F3.transformation(T3))
	['taille', 'age', 'poids'], [[160, 12, 40], [180, None, 80], [12, 14, 16]]
	"""

	def __init__(self,nom_variable,filtre):
		"""Initialise la classe"""
		super().__init__()
		self.nom_variable = nom_variable
		self.filtre = filtre

	
	def transformation(self, table=Tableau([],[[]])):
		"""selectionne les lignes pour lesquelles la condition du filtre est respectée

        Pour les exemples d'utilisation, voir la documentation de la classe Filtrage

        Parameters
        ----------
        table : Tableau
            tableau qui va subir le filtrage
        
        Returns
        -------
        Tableau
            table après le filtrage
        """
		t = Tableau(table.header,[])
		for i in range(len(self.nom_variable)):
			if self.nom_variable[i] not in table.header:
				return "La colonne spécifiée n'existe pas"  #Si une des variables spécifiée n'existe pas le programme s'arrête.
		for i in range(len(self.nom_variable)) :
			for j in range(len(table.header)) :    
				if self.nom_variable[i] == table.header[j] :
					for x in table.body:
						if x[j]== None :     #On fait le choix de renvoyer la ligne si il y a une NA (pour ne pas avoir de perte d'information.
							t.add_row(x)
						else :
							if self.filtre(x[j]) :
								t.add_row(x)
		return t


if __name__ == "__main__" :
    doctest.testmod(verbose=True)




