import doctest

class Tableau:
    """
    Classe implémentant des tableaux, caractérisés par un header et un body

    Attributes
    ----------
    header : list[str]
        liste des noms des différentes variables du tableau

    body : list[list]
        liste de listes des valeurs associées aux variables contenues dans le header 

    Examples
    --------

    >>> T = Tableau(['id','name'], [[1, 'ABC'],[2, 'DEF'], [3, 'GHI']])
    >>> print(T)
    ['id', 'name'], [[1, 'ABC'], [2, 'DEF'], [3, 'GHI']]
    >>> T = Tableau(["taille","age","poids"],[[160,12,40],[180,67,80],[12,14,16]])
    >>> T.add_header("sexe")
    >>> print(T)
    ['taille', 'age', 'poids', 'sexe'], [[160, 12, 40], [180, 67, 80], [12, 14, 16]]
    >>> T.add_column(["F", "F", "F"])
    >>> print(T)
    ['taille', 'age', 'poids', 'sexe'], [[160, 12, 40, 'F'], [180, 67, 80, 'F'], [12, 14, 16, 'F']]
    >>> T.add_row([170,20,60,'M'])
    >>> print(T)
    ['taille', 'age', 'poids', 'sexe'], [[160, 12, 40, 'F'], [180, 67, 80, 'F'], [12, 14, 16, 'F'], [170, 20, 60, 'M']]
    >>> T.del_row([170,20,60,'M']) 
    >>> print(T)
    ['taille', 'age', 'poids', 'sexe'], [[160, 12, 40, 'F'], [180, 67, 80, 'F'], [12, 14, 16, 'F']]
    >>> T.del_column("sexe")
    >>> print(T)
    ['taille', 'age', 'poids', 'sexe'], [[160, 12, 40], [180, 67, 80], [12, 14, 16]]
    >>> T.del_header("sexe")
    >>> print(T)
    ['taille', 'age', 'poids'], [[160, 12, 40], [180, 67, 80], [12, 14, 16]]
    >>> M = Tableau(["sexe"],[[]])
    >>> M.add_column(["F", "F", "F"])
    >>> print(M)
    ['sexe'], [['F'], ['F'], ['F']]

    """
  
    def __init__(self, header, body):
        """Initialise la classe"""
        self.header = header
        self.body = body

    def add_header(self,x):
        """Ajoute le nom d'une variable dans header
        Pour les exemples d'utilisation, voir la documentation de la classe Tableau

        Parameters
        ----------
        x : str
            nom de la nouvelle variable
        
        Returns
        -------
        Tableau
            la nouvelle table avec la nouvelle variable en plus
        """
        self.header.append(x)

    def add_column(self,L): 
        """Ajoute une nouvelle colonne

        Pour les exemples d'utilisation, voir la documentation de la classe Tableau

        Parameters
        ----------
        L : list
            liste des valeurs d'une nouvelle variable      
        Returns
        -------
        Tableau
            table avec la nouvelle colonne dans le body
        """
        if len(L) != len(self.body) :
            if self.body != [[]] :
                raise Exception("le nombre de ligne ne matche pas avec la taille de la liste")
            else : 
                self.body = [[L[i]] for i in range(len(L))]            
        else :
            for i in range(len(self.body)) :
                self.body[i].append(L[i])
    
    def add_row(self, row):
        """ajoute une ligne dans le body

        Pour les exemples d'utilisation, voir la documentation de la classe Tableau

        Parameters
        ----------
        row : list
            liste qui sera la nouvelle ligne du tableau
        
        Returns
        -------
        Tableau
            table avec la nouvelle ligne dans le body 
        """
        self.body.append(row)

    def del_header(self,x):
        """Supprime le nom d'une variable dans header
        Pour les exemples d'utilisation, voir la documentation de la classe Tableau

        Parameters
        ----------
        x : str
            nom de la variable que l'on veut supprimer
        
        Returns
        -------
        Tableau
            la nouvelle table sans la variable dans header
        """
        self.header.remove(x)

    def del_column(self, nom_colonne):
        """Supprime une colonne

        Pour les exemples d'utilisation, voir la documentation de la classe Tableau

        Parameters
        ----------
        nom_colonne : str
            nom de la variable pour laquelle on souhaite supprimer la colonne       
        Returns
        -------
        Tableau
            table sans la colonne dans le body
        """
        if nom_colonne not in self.header:
            raise("Erreur : La colonne n'existe pas")
        else: 
            num_col = self.header.index(nom_colonne)
            for i in range(len(self.body)):
                self.body[i].pop(num_col)
    
    def del_row(self, row):
        """supprime une ligne dans le body

        Pour les exemples d'utilisation, voir la documentation de la classe Tableau

        Parameters
        ----------
        row : list
            la ligne que l'on veut supprimer
        
        Returns
        -------
        Tableau
            table sans la ligne dans le body 
        """
        self.body.remove(row)
                   
    def __str__(self):
        return str(self.header) + ", " + str(self.body)

if __name__ == "__main__":
    doctest.testmod(verbose=True)
