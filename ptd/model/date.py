class Date:
    """Classe implémentant les dates, des années aux secondes.

    Attributes
    ----------
    annee : int
    mois : int
    jour : int
    heure : int
    minute : int
    seconde : int

    Examples
    --------
    >>> date=Date(2022, 05,27)
    
    >>> print(date.annee)
    2022
    """
    def __init__(self,annee,mois,jour,heure=0,minute=0,seconde=0):
        """Initialise la classe"""
        self.annee = annee
        self.mois = mois
        self.jour = jour
        self.heure = heure
        self.minute = minute 
        self.seconde = seconde