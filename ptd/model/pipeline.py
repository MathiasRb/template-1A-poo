from ptd.model.tableau import Tableau
import doctest
from ptd.transformations.centrage import Centrage
from ptd.transformations.selection import Selection

class Pipeline: 
    """
    Classe implémentant des pipelines. 
    Les pipelines permettre l'excécution à la suite de plusieurs transformations d'une table donnée

     Attributes
    ----------
    list_transfo : list
        liste d'instances de différentes transformations

    Examples
    --------
    >>> T=Tableau(["var"],[[0],[1],[2],[3],[4],[5],[6],[7],[8],[9]])
    >>> centre=Centrage("var")
    >>> S = Selection(['var_centré(e)'])
    >>> pipeline=Pipeline([centre,S])
    >>> print(pipeline.play(T))
    ['var_centré(e)'], [[-4.5], [-3.5], [-2.5], [-1.5], [-0.5], [0.5], [1.5], [2.5], [3.5], [4.5]]
    """
    def __init__(self, list_transfo):
        """Initialise la classe"""
        self.list_transfo=list_transfo

    def play(self, table=Tableau([],[[]])):
        """Renvoie la table après avoir subie toutes les transformations de la liste

        Pour les exemples d'utilisation, voir la documentation de la classe Pipeline

        Parameters
        ----------
        table : Tableau
            table pour laquelle on va lui appliquer les transformations de la liste en attribut
        
        Returns
        -------
        Tableau
            table finale
        """
        n=len(self.list_transfo)
        for i in range(n):
            table=self.list_transfo[i].transformation(table)
        return table


if __name__ == "__main__":
    doctest.testmod(verbose=True)