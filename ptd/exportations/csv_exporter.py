from ptd.exportations.exportation import Exportation
from ptd.model.tableau import Tableau

class CsvExporter(Exportation):
    """Classe permettant l'exportation d'instances Tableau en fichier CSV.

    Attributes
    ----------
    chemin : list[str]
        chemin complet représentant l'endroit où l'on veut exporter une instance de Tableau
     sep : str
        séparateur se trouvant entre deux valeurs du fichier CSV (par défault: ";")
    dec : str
        séparateur se trouvant, pour les valeurs numériques, entre le chiffre des unités et les décimales (par défault: ".")
    encodage: str
        définit l'encodage (par défault: "utf-8")

    """

    def __init__(self, chemin, sep = ";", dec = ".", encodage = "utf-8"):
        """Initialise la classe"""
        super().__init__(chemin)
        self.sep = sep
        self.dec = dec
        self.encodage = encodage

    def transformation(self, table=Tableau([],[[]])):
        """Permet l'exportation d'un Tableau en fichier CSV

        Parameters
        ----------
        table : Tableau
            instance de Tableau que l'on souhaite exporter
        
        Returns
        -------
        Tableau
            table définit en paramètre de la méthode (l'intérêt de renvoyer la même table réside dans 
            l'utilisation du pipeline: toutes les méthodes transformations renvoient un Tableau)
        """
        exp = open(self.chemin+"/export.csv", "w", encoding=self.encodage)
        exp.write(';'.join(table.header)+'\n')
        for ligne in table.body:
            exp.write(';'.join([str(x) for x in ligne])+'\n')
        exp.close()

        return table