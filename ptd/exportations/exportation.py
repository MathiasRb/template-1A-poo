from ptd.transformations.transformation import Transformation
from ptd.model.tableau import Tableau

class Exportation(Transformation):
    """Classe-mère abstraite implémentant l'exportation de fichiers.

    Attributes
    ----------
    chemin : list[str]
        chemin complet représentant l'endroit où l'on veut exporter une instance de Tableau

    """

    def __init__(self, chemin):
        """Initialise la classe"""
        super().__init__()
        self.chemin = chemin


    def transformation(self, table=Tableau([],[[]])):
        pass