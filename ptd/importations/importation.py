from ptd.transformations.transformation import Transformation
from ptd.model.tableau import Tableau

class Importation(Transformation): 
    """Classe-mère implémentant l'importation de fichiers.
    
    Attributes
    ----------
    chemin : list[str]
        chemin complet du fichier avec le nom du fichier et son extension, par exemple './data/2014-03.json.gz'
    valeur_manquante : str
        marqueur utilisé dans le fichier pour mentionner une valeur manquante (par exemple "NA", "mq", ou encore une chaîne de caractères vide : valeur par défaut)

    """
    def __init__(self,chemin,valeur_manquante = ""):
        """Initialise la classe"""
        super().__init__()
        self.chemin=chemin
        self.valeur_manquante=valeur_manquante

    def transformation(self,table=Tableau([],[[]])):
        """méthode abstraite"""
        pass
