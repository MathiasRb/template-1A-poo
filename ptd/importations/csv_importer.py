from numpy import rollaxis
from ptd.importations.importation import Importation 
from ptd.model.tableau import Tableau
import doctest
import gzip
import csv

class CsvImporter(Importation):
    """
    Classe implémentant l'importation de fichiers CSV

    Attributes
    ----------
    chemin : list[str]
        chemin complet du fichier avec le nom du fichier et son extension, par exemple './data/2014-03.csv.gz'
    valeur_manquante : str
        marqueur utilisé dans le fichier pour mentionner une valeur manquante (par exemple "NA", "mq", ou encore une chaîne de caractères vide : valeur par défaut)
    sep : str
        séparateur se trouvant entre deux valeurs du fichier CSV (par défault: ";")
    dec : str
        séparateur se trouvant, pour les valeurs numériques, entre le chiffre des unités et les décimales (par défault: ".")
    encodage: str
        définit l'encodage (par défault: "utf-8")
        
    Examples
    --------
    >>> CsvI = CsvImporter("./data/synop.201301.csv.gz", 'mq')
    >>> matable=CsvI.transformation()
    >>> print(matable.header[0:4])
    ['numer_sta', 'date', 'pmer', 'tend']
    >>> print(matable.body[0][0:13])
    ['07005', 20130101000000, 100290, -50, 8, 200, 6.7, 281.65, 280.45, 92, 14000, 61, None]
    """

    def __init__(self,chemin,valeur_manquante="",sep=";",dec=".",encodage="utf-8"):
        """Initialise la classe"""
        super().__init__(chemin, valeur_manquante)
        self.sep = sep
        self.dec = dec
        self.encodage = encodage 

    def transformation(self,table=Tableau([],[[]])):
        """Renvoie la table importée d'un fichier CSV

        Pour les exemples d'utilisation, voir la documentation de la classe CsvImporter

        Parameters
        ----------
        table : Tableau
            paramètre optionnel sans intérêt ici, mais présent car il s'agit d'un cas particulier de transformation où aucune table n'est nécessaire en entrée
        
        Returns
        -------
        Tableau
            table importée
        """
        data=[]
        with gzip.open(self.chemin, mode='rt') as gzfile:
            synopreader = csv.reader(gzfile,delimiter=self.sep)
            for row in synopreader:
                ligne=[]
                for element in row: 
                    if element==self.valeur_manquante:
                        ligne.append(None)
                    else:
                        try:
                            if element[0] == "0":
                                ligne.append(element)
                            elif float(element) % 1 != 0.0:
                                ligne.append(float(element))
                            else:
                                ligne.append(int(element))
                        except:
                            ligne.append(element)
                ligne.pop()
                data.append(ligne)
               
        return Tableau(data[0],data[1:]) 

if __name__ == "__main__":
    doctest.testmod(verbose=True)


