"""Ce module contient des utilitaires pour l'importation en JSON"""

def aplatir(dictionnaire, cle_depart="", sep='_'):
    """Fonction récursive qui renvoie un dictionnaire avec un seul niveau hiérarchique
    
    Les clés du dictionnaire renvoyé ne contiennent aucun dictionnaire comme valeur correspondante, seulement un float, int, str, bool ou None.

    Parameters
    ----------
    dictionnaire : dict
        dictionnaire que l'on souhaite "aplatir"
    cle_depart : str
        nom de la clé au premier niveau hiérarchique (par défaut "")
    sep : str
        séparateur utilisé pour noter les clés des sous-dictionnaires (par défaut "_", par exemple "fields_region" pour un sous-dictionnaire "fields" contenant une clé "région")
        
    Returns
    -------
    dict
        dictionnaire à un seul niveau hiérarchique : les clés ne peuvent pas avoir comme valeur correspondante un (sous-)dictionnaire
    """
    items = []
    for cle, valeur in dictionnaire.items():
        new_key = cle_depart + sep + cle if cle_depart else cle
        if isinstance(valeur, dict):
            items.extend(aplatir(valeur, new_key, sep=sep).items())
        else:
            items.append((new_key, valeur))
    return dict(items)


def harmoniser_et_trier(liste_dictionnaires):
    """Renvoie une liste contenant des valeurs manquantes partout où la clé n'a pas été mentionnée alors qu'elle existe ailleurs (dans d'autres dictionnaires)

    Parameters
    ----------
    liste_dictionnaires : list[dict]
        la liste de dictionnaires à laquelle on souhaite rajouter des valeurs manquantes
        
    Returns
    -------
    list[dict]
        la liste de dictionnaires contenant les valeurs manquantes sous forme de None
    """
    cles=set()
    nouvelle_liste = []

    #inventaire des cles
    for dictionnaire in liste_dictionnaires:
        for k in dictionnaire.keys():
            cles.add(k)

    #harmonisation avec valeurs manquantes quand il manque une clé
    for dictionnaire in liste_dictionnaires: 
                
        cles_manquantes = cles - set(dictionnaire.keys())
        items = []

        for cle in cles_manquantes:
            items.append((cle, None))
        for cle, valeur in dictionnaire.items():
            items.append((cle, valeur))

        dic = dict(items)
        nouvelle_liste.append(sorted(dic.items(), key=lambda t: t[0])) #tri par les clés

    return nouvelle_liste