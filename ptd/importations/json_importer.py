from ptd.importations.importation import Importation 
from ptd.model.tableau import Tableau
from ptd.importations.functions_json import *
import doctest
import gzip
import json

class JsonImporter(Importation):
    """Classe implémentant l'importation de fichiers JSON.

    Attributes
    ----------
    chemin : list[str]
        chemin complet du fichier avec le nom du fichier et son extension, par exemple './data/2014-03.json.gz'
    valeur_manquante : str
        marqueur utilisé dans le fichier pour mentionner une valeur manquante (par exemple "NA", "mq", ou encore une chaîne de caractères vide : valeur par défaut)

    Examples
    --------
    >>> import os
    >>> chemin = './data/2014-03.json.gz'
    >>> fichierJson = JsonImporter(chemin, "mq")
    >>> table = fichierJson.transformation()
    >>> print(table.header)
    ['datasetid', 'fields_code_insee_region', 'fields_consommation_brute_electricite_rte', 'fields_consommation_brute_gaz_grtgaz', 'fields_consommation_brute_gaz_terega', 'fields_consommation_brute_gaz_totale', 'fields_consommation_brute_totale', 'fields_date', 'fields_date_heure', 'fields_heure', 'fields_region', 'fields_statut_grtgaz', 'fields_statut_rte', 'fields_statut_terega', 'record_timestamp', 'recordid']
    >>> print(table.body[0])
    ['consommation-quotidienne-brute-regionale', '11', 7839, 10310, None, 10310, 18149, '2014-03-31', '2014-03-31T07:00:00+02:00', '07:00', 'Île-de-France', 'Définitif', 'Définitif', None, '2018-04-26T08:49:58.364+02:00', '2a6ef9093220ff0fce908bd55ad8675138d58844']
    """

    def __init__(self,chemin, valeur_manquante):
        """Initialise la classe"""
        super().__init__(chemin, valeur_manquante)

    def transformation(self,table=Tableau([],[[]])):
        """Renvoie la table importée d'un fichier JSON

        Pour les exemples d'utilisation, voir la documentation de la classe JsonImporter

        Parameters
        ----------
        table : Tableau
            paramètre optionnel sans intérêt ici, mais présent car il s'agit d'un cas particulier de transformation où aucune table n'est nécessaire en entrée
        
        Returns
        -------
        Tableau
            table importée
        """

        #Importation de la table
        with gzip.open(self.chemin, mode = 'rt', encoding='utf-8') as gzfile :
            data_init = json.load(gzfile)

        # Transformation d'un dictionnaire à plusieurs niveaux hiérarchiques (sous-dictionnaires emboîtés dans des dictionnaires) à un seul niveau hiérarchique (un seul dictionnaire)
        liste_dictionnaires = []
        for dictionnaire_a_niveaux in data_init:
            dictionnaire = aplatir(dictionnaire_a_niveaux)
            liste_dictionnaires.append(dictionnaire)

        # Gestion des valeurs manquantes
        liste_liste_items = harmoniser_et_trier(liste_dictionnaires)
        
        # Construction du tableau avec les données manquantes gérées et le dictionnaire réduit à un niveau hiérarchique
        cles = [liste_liste_items[0][i][0] for i in range(len(liste_liste_items[0]))]
        valeurs = []
        for liste_items in liste_liste_items:
            valeur = []
            for x in liste_items:
                if x[1] == self.valeur_manquante:
                    valeur.append(None)
                else:
                    valeur.append(x[1])
            valeurs.append(valeur)

        return Tableau(cles, valeurs)

if __name__ == "__main__":
    doctest.testmod(verbose=True)