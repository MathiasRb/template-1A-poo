import csv
from ptd.model.pipeline import Pipeline
from ptd.model.date import Date
from ptd.model.tableau import Tableau

from ptd.importations.csv_importer import CsvImporter
from ptd.importations.json_importer import JsonImporter

from ptd.transformations.agregation_spatiale import AgregationSpatiale
from ptd.transformations.centrage import Centrage
from ptd.transformations.fenetrage import Fenetrage
from ptd.transformations.filtrage import Filtrage
from ptd.transformations.jointure import Jointure
from ptd.transformations.moyenne_glissante import MoyenneGlissante 
from ptd.transformations.normalisation import Normalisation
from ptd.transformations.selection import Selection

from ptd.exportations.csv_exporter import CsvExporter

# Nous avons ici testé toutes nos classes dans une procédure d'import-transformation-exportation classique
if __name__ == "__main__":

    # Exemple avec un fichier CSV.GZ

    T1=CsvImporter("./data/synop.201301.csv.gz", 'mq')
    T2=Selection(['numer_sta','date','tend'])
    T3=Fenetrage('date',Date(2013,1,5),Date(2013,1,5,8))
    T4=Filtrage(['numer_sta'], lambda x:x in ['07072', '07335', '07790', '07747'])
    T5=AgregationSpatiale(['date', 'tend'],[lambda x: str(x[0])[:8], max],"numer_sta")
    T6=Centrage("tend")
    T7=Normalisation("tend")
    T8=MoyenneGlissante(3, "tend")

    # on importe le fichier CSV contenant les postes météo et leurs régions (l'import en csv non compressé ne fait pas partie des fonctionnalités disponibles dans les transformations)
    data = []
    with open("./data/postes_avec_Regions.csv", mode='rt', encoding="utf-8") as file:
        read = csv.reader(file,delimiter=";")
        for row in read:
            ligne=[]
            for element in row: 
                ligne.append(element)
            data.append(ligne)
    postes = Tableau(data[0],data[1:])

    T9=Jointure(postes, "numer_sta", "ID")
    T10=Selection(["date", 'tend','numer_sta', 'tend_centré(e)', 'tend_normalisé(e)', 'moy_gliss_tend', 'Region'])
    T11=CsvExporter("./data/")

    L1 = [T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11]

    pipeline_1 = Pipeline(L1)
    tableau_1 = pipeline_1.play()
    print("Résultat pour le premier pipeline : ")
    print(tableau_1)

    # Exemple avec un fichier JSON.GZ

    T12=JsonImporter('./data/2014-03.json.gz', "mq")
    T13=Selection(['fields_consommation_brute_gaz_totale','fields_date_heure','fields_region'])
    T14=Filtrage(["fields_date_heure"], lambda x:x=="2014-03-21T06:00:00+01:00")
    T15=Filtrage(["fields_region"], lambda x:x[0]=="B")
    
    L2 = [T12, T13, T14, T15]

    pipeline_2 = Pipeline(L2)
    tableau_2 = pipeline_2.play()
    print("Résultat pour le deuxième pipeline : ")
    print(tableau_2)